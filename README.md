# eyureka

<br>
Eyureka API project

# How to Start (development local):

  - create virtual environment python3.6 at root folder `python3.6 -m venv venv`
  - Acitvated python virtual env with this command `$ source venv/bin/activate`
  - Install dependency library for project shark with run this command `pip install -r requirements.txt`
  - Run this command `python manage.py migrate`
  - Rename `env.example` to `.env`
  - Run this command to start server `python manage.py runserver 0.0.0.0:8000
  - Check your browser and type this address `http://localhost:8000`
  
 # How to Start (Docker):

  - Copy `.env.examlple to .env`
  - Run this command `docker-compose build`
  - Run this command `docker-compose up -d`
  - check log each container : `docker container logs django01`, `docker container logs nginx01`, `docker container logs postgres01`
  - Open browser: `localhost:8000`
  
  
# Technology:

| **Tech** | **Ver** | **Description** |**Learn More**|
|----------|----------|-------|---|
|[Django](https://docs.djangoproject.com/en/1.11/)|1.11.4|Current python framework|https://docs.djangoproject.com/en/1.11/|
|[psycopg2](http://pythonhosted.org/psycopg2/)|2.7.3|PostgreSQL adapter for connection database|http://pythonhosted.org/psycopg2/|
|[django-environ](https://github.com/joke2k/django-environ)|0.4.4|Django environtment|https://github.com/joke2k/django-environ|
|