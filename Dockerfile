# pull official base image
FROM python:3.6
LABEL maintainer="riyadhprjct@gmail.com"

# set work directory
WORKDIR /usr/src/app

ENV PYTHONUNBUFFERED 1

COPY ./ ./

RUN pip install -r requirement.txt