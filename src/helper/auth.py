import datetime
import hashlib
import random
import string
import uuid

import jwt
import pytz
from django.utils.dateparse import parse_datetime
from django.utils.datetime_safe import strftime
from django.utils.timezone import now
from rest_framework import status

from eyureka import settings
from src.account.models import Contact, AccountContact
from src.group.models import Role
from src.helper.helpers import get_object_or_none
from src.helper.response import response


def hash_password(password):
    # uuid is used to generate a random number
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ':' + salt

def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()

def generate_token(acc_contact):
    payload = {
        "expiresOn" : strftime(now() + datetime.timedelta(1), '%Y-%m-%dT%H:%M:%S%z'),
        "account": {
            "id": acc_contact.account.id,
            "name": acc_contact.account.name,
        },
        "contact": {
            "id": acc_contact.contact.id,
            "name": acc_contact.contact.name,
        }
    }

    return {
        "token": jwt.encode(payload, key=settings.JWT_SECRET_KEY, algorithm='HS256').decode('utf-8'),
        "refreshToken": ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(40)),
    }

def login_required(function, module="", access=""):
    def wrapper(request, *args, **kw):
        auth = request.request._request.META.get('HTTP_AUTHORIZATION')
        if not auth:
            return response(status.HTTP_401_UNAUTHORIZED, message="Unauthorize (Token is required")

        auth_split = auth.split()

        #jwt token
        try:
            token_decode = jwt.decode(auth_split[1], settings.JWT_SECRET_KEY, verify=False)
        except:
            return response(401, message="Invalid token")

        if datetime.datetime.now(tz=pytz.timezone(settings.TIME_ZONE)) > parse_datetime(token_decode['expiresOn']):
            return response(401, message="JWT expired")

        acc_contact = get_object_or_none(AccountContact, contact__id=token_decode['contact']['id'])
        if not acc_contact:
            return response(401, message="Unauthorized (User not found)")
        allowed = get_object_or_none(Role, group=acc_contact.contact.group_id, module=module, access=access)
        if not allowed:
            return response(405, message="You do not have for {} module".format(access, module))

        kw.update({
            "account": acc_contact.account, "accountId": acc_contact.account.id,
            "contact": acc_contact.contact, "contactId": acc_contact.contact.id
        })

        return function(request, *args, **kw)

    return wrapper

def generate_password():
    return uuid.uuid4().__str__()