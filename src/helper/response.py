from django.http import JsonResponse

from src.helper.helpers import decodeErrors


def response(code, data=[], message='', status=False, meta=None, errors=[], sys_errors=None):
    if meta == None and code == 200:
        meta = {
            "page": 0,
            "limit": 0,
            "totalPages": 0,
            "totalRecords": 0
        }
    res_errors = []
    if errors:
        res_errors = decodeErrors(errors)
    else:
        if sys_errors:
            message = sys_errors

    res = {
        'code': code,
        'data': data,
        'meta': meta,
        'message': message,
        'errors': res_errors,
        'success': status
    }

    return JsonResponse(res, status=code)