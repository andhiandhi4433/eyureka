def decodeErrors(dataErrors):
    errors = []
    if dataErrors:
        if isinstance(dataErrors, list):
            for error_detail in dataErrors:
                for key, value in error_detail.items():
                    if isinstance(value, list):
                        value = str(value[0])
                    item = {
                        "field": key,
                        "message": value
                    }
                    errors.append(item)
        else:
            for key, value in dataErrors.items():
                if isinstance(value, list):
                    value = str(value[0])
                item = {
                    "field": key,
                    "message": value
                }
                errors.append(item)

    return errors


def get_object_or_none(obj, **kwargs):
    try:
        return obj.objects.get(**kwargs)
    except:
        return None


def add_modified_by(data, **kwargs):
    if isinstance(data, dict):
        data.update({'modifiedBy': kwargs.get('contact')})
        return data

    return data


def add_created_modified_by(data, **kwargs):
    contact = kwargs.get('contact')
    if isinstance(data, dict):
        data.update({'createdBy': contact, 'modifiedBy': contact})
        return data

    return data