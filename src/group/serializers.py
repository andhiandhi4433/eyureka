from rest_framework import serializers

from src.account.models import Account, AccountGroup
from src.group.models import Group, Role, Module


class GroupSerializers(serializers.Serializer):
    """
    Create and return a new 'User Role' instance, given the validated data.
    """

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=50)
    description = serializers.CharField(max_length=150)

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.IntegerField(required=False, source="created_by", allow_null=True)
    modifiedBy = serializers.IntegerField(required=False, source="modified_by", allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `User Role` instance, given the validated data.
        """

        return Group.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `User Role` instance, given the validated data.
        """

        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)

        instance.modified_by = validated_data.get('modified_by', instance.modified_by)
        instance.save()

        return instance


    def validate(self, attrs):
        if not self.initial_data['roles']:
            raise serializers.ValidationError({'roles': 'roles is required'})

        return attrs


class ModuleSerializers(serializers.Serializer):
    """
    Create and return a new 'Module' instance, given the validated data.
    """

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=50)
    description = serializers.CharField(max_length=150)

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.IntegerField(required=False, source="created_by", allow_null=True)
    modifiedBy = serializers.IntegerField(required=False, source="modified_by", allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `User Role` instance, given the validated data.
        """

        return Module.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `User Role` instance, given the validated data.
        """

        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)

        instance.modified_by = validated_data.get('modified_by', instance.modified_by)
        instance.save()

        return instance


    def validate(self, attrs):
        if not self.initial_data['rules']:
            raise serializers.ValidationError({'rules': 'rules is required'})

        return attrs


class RoleSerializers(serializers.Serializer):
    """
    Create and return a new 'Role' instance, given the validated data.
    """

    id = serializers.IntegerField(read_only=True)
    module = serializers.PrimaryKeyRelatedField(queryset=Module.objects.all())
    access = serializers.ChoiceField(choices=Role.ACCESS)
    group = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all(), required=False)

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.IntegerField(required=False, source="created_by", allow_null=True)
    modifiedBy = serializers.IntegerField(required=False, source="modified_by", allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `User Role` instance, given the validated data.
        """

        return Role.objects.create(**validated_data)


class AccountGroupSerializers(serializers.Serializer):
    account = serializers.PrimaryKeyRelatedField(queryset=Account.objects.all())
    group = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all())

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.IntegerField(required=False, source="created_by", allow_null=True)
    modifiedBy = serializers.IntegerField(required=False, source="modified_by", allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `Account Group` instance, given the validated data.
        """

        return AccountGroup.objects.create(**validated_data)