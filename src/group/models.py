from django.db import models

# Create your models here.
class Group(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'group'

    def __str__(self):
        return self.name


class Module(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'module'

    def __str__(self):
        return self.name


class Role(models.Model):
    ACCESS = [
        ('CREATE', 'CREATE'),
        ('READ', 'READ'),
        ('UPDATE', 'UPDATE'),
        ('DELETE', 'DELETE')
    ]


    group = models.ForeignKey(Group, on_delete=models.PROTECT)
    module = models.ForeignKey(Module, on_delete=models.PROTECT)
    access = models.CharField(choices=ACCESS, max_length=20)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'role'

    def __str__(self):
        return self.name


