from django.conf.urls import url

from .views import group

urlpatterns = [
    url(r'^group$', group.GroupView.as_view(), name='master_group'),
]