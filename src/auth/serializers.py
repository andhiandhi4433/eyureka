from rest_framework import serializers


class AuthSerializers(serializers.Serializer):
    ACTION_GET_TOKEN = 'GET_TOKEN'
    ACTION_REF_TOKEN = 'REFRESH_TOKEN'
    action_choices = (
        (ACTION_GET_TOKEN, ACTION_GET_TOKEN),
        (ACTION_REF_TOKEN, ACTION_REF_TOKEN),
    )
    email = serializers.EmailField()
    password = serializers.CharField(max_length=255)
    action = serializers.ChoiceField(choices=action_choices)
    token = serializers.CharField(required=False, allow_blank=True)

    def validate(self, attrs):
        attrs['email'] = attrs['email'].lower()

        if attrs['action'] == 'REFRESH_TOKEN' and not attrs.get('token'):
            raise serializers.ValidationError({'token': 'Token is required'})
        if attrs['action'] == 'REFRESH_TOKEN' and not attrs.get('refreshToken'):
            raise serializers.ValidationError({'token': 'refreshToken is required'})

        if attrs['action'] == 'GET_TOKEN' and not attrs.get('email'):
            raise serializers.ValidationError({'email': 'Email is required'})
        if attrs['action'] == 'GET_TOKEN' and not attrs.get('password'):
            raise serializers.ValidationError({'password': 'Password is required'})

        return attrs
