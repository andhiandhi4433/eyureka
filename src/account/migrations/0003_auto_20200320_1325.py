# Generated by Django 2.2.10 on 2020-03-20 13:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20200318_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='accountgroup',
            name='is_delete',
            field=models.BooleanField(default=False),
        ),
    ]
