from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^activate$', views.Activate.as_view(), name='login'),
    url(r'^contact/invite/(?P<account_id>[\w\-]+)$', views.Invite.as_view(), name='invite_contact'),
]