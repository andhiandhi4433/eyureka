from rest_framework import serializers

from src.account.models import Account, Contact, AccountContact
from src.group.models import Group
from src.helper.auth import hash_password


class AccountSerializers(serializers.Serializer):
    id = serializers.CharField(max_length=64, read_only=True)
    name = serializers.CharField(max_length=100)
    npwpNo = serializers.CharField(max_length=20, source="npwp_no")
    address = serializers.CharField(max_length=255)
    isActive = serializers.BooleanField(source="is_active", required=False)

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.PrimaryKeyRelatedField(required=False, source="created_by", queryset=Account.objects.all())
    modifiedBy = serializers.PrimaryKeyRelatedField(required=False, source="modified_by", queryset=Account.objects.all())

    def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """

        return Account.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `User` instance, given the validated data.
        """

        instance.name = validated_data.get('name', instance.name)
        instance.npwp_no = validated_data.get('npwp_no', instance.npwp_no)
        instance.address = validated_data.get('address', instance.address)
        instance.is_active = validated_data.get('is_active', instance.is_active)

        instance.modified_by = validated_data.get('modified_by', instance.modified_by)
        instance.save()

        return instance


class ContactSerializers(serializers.Serializer):
    id = serializers.CharField(max_length=64, read_only=True)
    group = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all(), required=False)
    name = serializers.CharField(max_length=100)
    email = serializers.EmailField()
    password = serializers.CharField(max_length=255, required=False)

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.PrimaryKeyRelatedField(required=False, source="created_by", queryset=Account.objects.all())
    modifiedBy = serializers.PrimaryKeyRelatedField(required=False, source="modified_by", queryset=Account.objects.all())

    def create(self, validated_data):
        """
        Create and return a new `User` instance, given the validated data.
        """

        return Contact.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `User` instance, given the validated data.
        """

        instance.account = validated_data.get('account', instance.account)
        instance.group = validated_data.get('group', instance.group)
        instance.name = validated_data.get('name', instance.name)
        instance.email = validated_data.get('email', instance.email)
        instance.password = validated_data.get('password', instance.password)

        instance.modified_by = validated_data.get('modified_by', instance.modified_by)
        instance.save()

        return instance

    def validate(self, attrs):
        attrs["email"] = attrs["email"].lower()
        if attrs.get("password"):
            attrs["password"] = hash_password(attrs["password"])

        return attrs


class AccountContactSerializers(serializers.Serializer):
    account = serializers.PrimaryKeyRelatedField(queryset=Account.objects.all())
    contact = serializers.PrimaryKeyRelatedField(queryset=Contact.objects.all())

    isDelete = serializers.BooleanField(source="is_delete", required=False)
    createdAt = serializers.DateTimeField(required=False, source="created_at")
    modifiedAt = serializers.DateTimeField(required=False, source="modified_at")
    createdBy = serializers.IntegerField(required=False, source="created_by", allow_null=True)
    modifiedBy = serializers.IntegerField(required=False, source="modified_by", allow_null=True)

    def create(self, validated_data):
        """
        Create and return a new `Account Group` instance, given the validated data.
        """

        return AccountContact.objects.create(**validated_data)


