from django.db import models

# Create your models here.
from src.group.models import Group


class Account(models.Model):
    name = models.CharField(max_length=100)
    npwp_no = models.CharField(max_length=20)
    address = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'account'

    def __str__(self):
        return self.name


class Contact(models.Model):
    group = models.ForeignKey(Group, on_delete=models.PROTECT, null=True)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    password = models.CharField(max_length=255)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'contact'

    def __str__(self):
        return self.name


class AccountContact(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    contact = models.ForeignKey(Contact, on_delete=models.PROTECT)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'account_contact'

    def __str__(self):
        return self.name


class AccountGroup(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    group = models.ForeignKey(Group, on_delete=models.PROTECT)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'account_group'

    def __str__(self):
        return self.name
