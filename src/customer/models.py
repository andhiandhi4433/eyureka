from django.db import models

# Create your models here.
from src.account.models import Account


class Client(models.Model):
    account = models.ForeignKey(Account, on_delete=models.PROTECT)
    paid_until = models.DateTimeField()
    on_trial = models.BooleanField(default=False)

    is_delete = models.BooleanField(default=False)
    created_by = models.CharField(max_length=50, null=True)
    modified_by = models.CharField(max_length=50, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'client'

    def __str__(self):
        return self.name